import {Component} from "react";
import fotoProfil from "../Components/nahar2.jpg";

export default class Resume extends Component {
    render () {
    return <div>
            <body>
                <div class= "box">
                    <div class= "sidebar">
                            <div class="biodata">
                                <div class="background-profile">
                                <img src={fotoProfil}/>
                                </div>
                                <button class="btn"><a>Download CV</a></button>
                            </div>
                    </div>
                    <div class ="main">
                        <div class="intro">
                            <div>
                                <h2>About Me</h2>
                            </div>
                            <p><b>A language nerd and tech-savvy who participated in the various web development project for a Non-Profit Organization. Completed the Intensive Bootcamp from Glints Academy focusing on Front-end development. Actively write articles for some reputable academic journals in the field of language and education. Adept at problem solving, time-management. Proficient in MS Office and Corel Draw.</b></p>
                            <div>
                                <hr/>
                                <h2>What I am doing</h2>
                            </div>
                        </div>
                        <div class="skill">
                            <div class="skill-logo skill-size">
                                <h2>Logo Design</h2>
                                <p>The most modern and high-quality design made at a professional level. Show off your brand’s personality with a custom promotion logo designed just for you by a professional designer</p>
                            </div>
                            <div class="skill-web skill-size">
                                <h2>Web Development</h2>
                                <p>High-quality development of sites at the professional level with the globally recognized web design and marketing company,</p>
                            </div>
                            <div class="skill-translate skill-size">
                                <h2>Translation</h2>
                                <p>The high-level translation which provides the affordable prices</p>
                            </div>
                            <div class="skill-english skill-size">
                                <h2>English Course</h2>
                                <p>Learn all about the English language and gain an accredited certification from top language coaching expert</p>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
            <div className="button">
                <button onClick={()=> this.props.history.push("/About")}>BACK</button>
                <button onClick={()=> this.props.history.push("/Blog")}>NEXT</button>
            </div>            
        </div>;
    }
}