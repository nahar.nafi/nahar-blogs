import React from 'react';
import { Jumbotron, Button } from 'reactstrap';
const Blog = (props) => {
  // console.log(props);
    const next = () => {
        props.history.push("/Home") 
    }
    return (
      <div>
        <Jumbotron>
          <h1 className="display-2">Hello, world!</h1>
          <p className="lead">This is a simple hero unit, a simple Jumbotron-style component for calling extra attention to featured content or information.</p>
          <hr className="my-2" />
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p className="lead">
            <Button color="primary">Learn More</Button>
          </p>
        </Jumbotron>
        <div className="button">
            <button onClick={next}>GO BACK TO HOME</button>
        </div>
      </div>
    );
  };
  
  export default Blog;