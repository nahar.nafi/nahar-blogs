import React from 'react';
import { Spinner } from 'reactstrap';

export default function Home() {
    return (
        <div className = "home">
            <h1 className="welcomeHome">WELCOME</h1>
            <h4 className="noteHome">This blog is a personal website which contains my daily acticity as a teacher. Through this blog I am going to share to all of you everything fun being a teacher</h4>
            <div style={{display:"flex", justifyContent:"center"}}>
                <Spinner type="grow" color="primary" />
                <Spinner type="grow" color="secondary" />
                <Spinner type="grow" color="success" />
                <Spinner type="grow" color="danger" />
                <Spinner type="grow" color="warning" />
                <Spinner type="grow" color="info" />
                <Spinner type="grow" color="light" />
                <Spinner type="grow" color="dark" />
            </div>
        </div>
    )
}

// const Spin = (props) => {
//     return (
//       <div>
//         <Spinner type="grow" color="primary" />
//         <Spinner type="grow" color="secondary" />
//         <Spinner type="grow" color="success" />
//         <Spinner type="grow" color="danger" />
//         <Spinner type="grow" color="warning" />
//         <Spinner type="grow" color="info" />
//         <Spinner type="grow" color="light" />
//         <Spinner type="grow" color="dark" />
//       </div>
//     );
//   }
  
//   export default Spin;