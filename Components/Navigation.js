import React from 'react'
import{Link} from "react-router-dom";

export default function Navigation() {
    return (
    <div className="navigation">
        <Link to="/"><h5>Home</h5></Link>
        <Link to="/About"><h5>About</h5></Link>
        <Link to="/Resume"><h5>Resume</h5></Link>
        <Link to="/Blog"><h5>Blog</h5></Link>
    </div>
    )
}


