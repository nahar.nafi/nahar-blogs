import {BrowserRouter as Router, Route, Switch, Link} from "react-router-dom";
import React from "react";
import Home from "./Pages/Home";
import About from "./Pages/About";
import Resume from "./Pages/Resume";
import Blog from "./Pages/Blog";
import Navigation from "./Components/Navigation";

function App() {
  return (
    <Router>
        <div>
          <Navigation/>
        </div>
        <div>
          <Switch>
            <Route component={Home} path="/" exact ={true} />
            <Route component={About} path="/About" />
            <Route component={Resume} path="/Resume" />
            <Route component={Blog} path="/Blog" />
            <Home/>
          </Switch>
        </div>
    </Router>
  );
}

export default App;
